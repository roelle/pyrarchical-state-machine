"""Heirarchical state machine module.

This module provides a framework for a responsive heirarchical state-machine.

For hybrid dynamical systems, the following update equations are commonly used.
```
x[k+1] = f_s(x[k], u(k]))
y[k] = g_s(x[k], u(k]))
```
where f and g are functions specific to each discrete state s, x is a
continuous state that exists in all discrete states, u is a set of inputs, and
y is the output of the system.

In a Python HSM implementation, only the during state will really obey the
discrete-time step, k. However, we may need to update states during other HSM
actions.
```
x = init_s(x, u)
x = entry_s(x, u)
x = during_s(x, u)
x = exit_s(x, u)
y = output_s(x, u)
```
The continuous state, x, is a member variable of the HSM. The output function
is a static method. The input, continuous state, and output each need an
object defined to describe them. At the simplest, this will just be a list.

The module will run a high level test if executed.
    $ python3 hsm.py

Copyright Matt Roelle 2018
"""

from enum import Enum
import logging

_log = logging.getLogger("hsm")

class ContinuousState(list):
    pass

class ContinuousInput(list):
    pass

class ContinuousOutput(list):
    pass

class Event(Enum):
    """State-machine event"""

    def __str__(self):
        return self.value

class Transition():
    """Transition in a heirarchical state-machine"""

    def __init__(self, name="null", event=None, target=None, action=None, guard=None):
        """Initialize the state"""
        self._name = name
        self.event = event
        self.target = target
        self.action = action
        self.guard = guard

class State():
    """State in a heirarchical state-machine"""

    def __init__(self, name="null"):
        """Initialize the state"""
        self._name = name
        self.parent = None
        self.init = None
        self.transitions = None
        self.init_action = self.default_init_action
        self.entry_action = self.default_entry_action
        self.during_action = self.default_during_action
        self.exit_action = self.default_exit_action
        self.output = self.default_output

    def __str__(self):
        return "State " + self._name
    
    def configure(self, parent=None, init=None, transitions=None):
        self.parent = parent
        self.init = init
        self.transitions = transitions

    def default_init_action(self, x : ContinuousState=None, u: ContinuousInput=None):
        _log.info(self._name + " : Init")
        return x

    def default_entry_action(self, x : ContinuousState=None, u: ContinuousInput=None):
        _log.info(self._name + " : Entry")
        return x

    def default_during_action(self, x : ContinuousState=None, u: ContinuousInput=None):
        _log.info(self._name + " : During")
        return x

    def default_exit_action(self, x : ContinuousState=None, u: ContinuousInput=None):
        _log.info(self._name + " : Exit")
        return x

    def default_output(self, x : ContinuousState=None, u: ContinuousInput=None):
        _log.info(self._name + " : Output")
        return x

    def first_child(self, state):
        """Recurse the parentage of a target state to find the first child."""
        if state is None:
            return None
        if state.parent is None:
            return None
        if state.parent is self:
            return state
        return self.first_child(state.parent)

    def find_target(self, event=None, x=None, u=None):
        """Recurse parental lineage until transition is found."""
        for transition in self.transitions:
            if event == transition.event:
                if transition.guard is None:
                    return self, transition.target
                elif transition.guard(x, u):
                    return self, transition.target
            if event is None and transition.guard is not None:
                if transition.guard(x, u):
                    return self, transition.target
        if isinstance(self.parent, State):
            return self.parent.find_target(event)
        return None, None

    def seek_state(self, state, x, u):
        child = self.first_child(state)
        if child is None:
            _log.debug("Go up to " + str(self.parent))        
            x = self.exit_action(x, u)
            return self.parent, x
        else:
            x = child.entry_action(x, u)
            return child, x

    def run_init(self, x, u):
        _log.debug("Run the init for " + str(self))        
        if self.init is None:
            return self, x, None
        else:
            x = self.init_action(x, u)
            x = self.init.entry_action(x, u)
            return self.init, x, self.init

class HSM():
    
    _top = State("top")
    __state = None # What is the "active" state
    x = ContinuousState()

    def __init__(self, initial_state=None):
        """Initialize the heirarchical state machine."""
        assert initial_state is not None, "Attempting HSM initialization but state is null"

        self.__state = initial_state
        self.x = self.__state.entry_action(self.x, None)
        while True:
            self.__state, self.x, target_state = self.__state.run_init(self.x, None)
            if target_state is None: break

    def dispatch(self, event : Event, u : ContinuousInput=None):
        """Process events with the state machine."""
        _log.info("state: " + self.__state._name)
        _log.info("event: " + str(event))
        event_state, target_state = self.__state.find_target(event, self.x, u)
        if target_state is None:
            return

        _log.debug("Go to the state with the transition : " + str(event_state))
        while self.__state is not event_state:
            self.__state, self.x = self.__state.seek_state(event_state, self.x, u)

        _log.debug("Transition from here : " + str(self.__state))
        while True:
            self.__state, self.x = self.__state.seek_state(target_state, self.x, u)
            if self.__state is target_state: break 

        _log.debug("Run inits.")
        while target_state is not None:
            self.__state, self.x, target_state = self.__state.run_init(self.x, u)

        _log.debug("Check for auto-transitions")
        self.dispatch(None, u)

    @property
    def state(self):
        """Get the active state."""
        return self.__state

    def execute_during_action(self, u : ContinuousInput=None):
        self.__state.during_action(self.x, u)


if __name__ == "__main__":

    import hsm

    class S211(hsm.State):

        def default_exit_action(self, x, u):
            x[0] += 1
            _log.info(self._name + " : Exit")
            return x

    class Events(hsm.Event):
        A = "a"
        B = "b"
        C = "c"
        D = "d"
        E = "e"
        F = "f"
        G = "g"
        H = "h"

    class Example(hsm.HSM):

        s0 = hsm.State("s0")
        s1 = hsm.State("s1")
        s11 = hsm.State("s11")
        s2 = hsm.State("s2")
        s21 = hsm.State("s21")
        s211 = S211("s211")

        def __init__(self):
            self.x.append(0)
            self.s0.configure(init=self.s1, \
                              transitions=(hsm.Transition(event=Events.E, \
                                                          target=self.s211),))
            self.s1.configure(parent=self.s0, \
                            init=self.s11, \
                            transitions=(hsm.Transition(event=Events.A, \
                                                        target=self.s1), \
                                         hsm.Transition(event=Events.B, \
                                                        target=self.s11), \
                                         hsm.Transition(event=Events.C, \
                                                        target=self.s2), \
                                         hsm.Transition(event=Events.D, \
                                                        target=self.s0), \
                                         hsm.Transition(event=Events.F, \
                                                        target=self.s211)))
            self.s11.configure(parent=self.s1, \
                            transitions=(hsm.Transition(event=Events.G, \
                                                          target=self.s211),))
            self.s2.configure(parent=self.s0, \
                            init=self.s21, \
                            transitions=(hsm.Transition(event=Events.C, \
                                                        target=self.s1), \
                                        hsm.Transition(event=Events.F, \
                                                        target=self.s11)))
            self.s21.configure(parent=self.s2, \
                            init=self.s211, \
                            transitions=(hsm.Transition(event=Events.B, \
                                                        target=self.s211), \
                                        hsm.Transition(event=Events.H, \
                                                        target=self.s21)))
            self.s211.configure(parent=self.s21, \
                            transitions=(hsm.Transition(event=Events.B, \
                                                        target=self.s21), \
                                        hsm.Transition(event=Events.D, \
                                                        target=self.s21), \
                                        hsm.Transition(event=Events.G, \
                                                        target=self.s0), \
                                        hsm.Transition(guard=self.my_guard, \
                                                        target=self.s11)))
            self.s21.entry_action = lambda x, u: self.my_action(self.s21, x, u)
            super().__init__(self.s0)
        
        @staticmethod
        def my_action(state : State, x, u):
            x[0] += 1
            state.default_entry_action()
            return x

        @staticmethod
        def my_guard(x, u):
            if float(x[0]) > 6:
                return True
            return False
        
    # logging.basicConfig(level=logging.INFO)
    logging.basicConfig(filename="hsm.log", filemode="w", level=logging.INFO)

    example_hsm = Example()
    
    example_hsm.dispatch(Events.A)
    example_hsm.dispatch(Events.E)
    example_hsm.dispatch(Events.B)
    example_hsm.dispatch(Events.E)
    example_hsm.dispatch(Events.G)
    example_hsm.dispatch(Events.E)
    example_hsm.dispatch(Events.B)
    # example_hsm.dispatch(Events.A)
    # example_hsm.dispatch(Events.H)
    # example_hsm.dispatch(Events.G)
    # example_hsm.dispatch(Events.B)
    # example_hsm.dispatch(Events.B)
    # example_hsm.dispatch(Events.C)
    # example_hsm.dispatch(Events.D)
    # example_hsm.dispatch(Events.E)
    # example_hsm.dispatch(Events.F)
    # example_hsm.dispatch(Events.G)
    # example_hsm.dispatch(Events.H)
    example_hsm.execute_during_action()
    _log.info('x : ' + str(example_hsm.x))