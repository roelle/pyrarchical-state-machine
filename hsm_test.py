import unittest
import hsm
from hashlib import md5

class S211(hsm.State):

    def default_exit_action(self, x, u):
        hsm._log.info(self._name + " : Exit")
        return x
 
class Events(hsm.Event):
    A = "a"
    B = "b"
    C = "c"
    D = "d"
    E = "e"
    F = "f"
    G = "g"
    H = "h"

class Example(hsm.HSM):

    s0 = hsm.State("s0")
    s1 = hsm.State("s1")
    s11 = hsm.State("s11")
    s2 = hsm.State("s2")
    s21 = hsm.State("s21")
    s211 = S211("s211")

    def __init__(self):
        self.x.append(md5())
        self.s0.configure(parent=None, \
                          init=self.s1, \
                          transitions=(hsm.Transition(event=Events.E, \
                                                      target=self.s211),))
        self.s1.configure(parent=self.s0, \
                          init=self.s11, \
                          transitions=(hsm.Transition(event=Events.A, \
                                                      target=self.s1), \
                                       hsm.Transition(event=Events.B, \
                                                      target=self.s11), \
                                       hsm.Transition(event=Events.C, \
                                                      target=self.s2), \
                                       hsm.Transition(event=Events.D, \
                                                      target=self.s0), \
                                       hsm.Transition(event=Events.F, \
                                                      target=self.s211)))
        self.s11.configure(parent=self.s1, \
                           transitions=(hsm.Transition(event=Events.G, \
                                                       target=self.s211),))
        self.s2.configure(parent=self.s0, \
                          init=self.s21, \
                          transitions=(hsm.Transition(event=Events.C, \
                                                      target=self.s1), \
                                       hsm.Transition(event=Events.F, \
                                                      target=self.s11)))
        self.s21.configure(parent=self.s2, \
                           init=self.s211, \
                           transitions=(hsm.Transition(event=Events.B, \
                                                       target=self.s211), \
                                        hsm.Transition(event=Events.H, \
                                                       target=self.s21)))
        self.s211.configure(parent=self.s21, \
                            transitions=(hsm.Transition(event=Events.B, \
                                                        target=self.s21), \
                                         hsm.Transition(event=Events.D, \
                                                        target=self.s21), \
                                         hsm.Transition(event=Events.G, \
                                                        target=self.s0), \
                                         hsm.Transition(guard=self.s211_s11_guard, \
                                                        target=self.s11)))

        self.s0.init_action = lambda x, u: self.update(self.x, b"s0_init")
        self.s0.entry_action = lambda x, u: self.update(self.x, b"s0_entry")
        self.s0.exit_action = lambda x, u: self.update(self.x, b"s0_exit")
        self.s0.during_action = lambda x, u: self.update(self.x, b"s0_during")

        self.s1.init_action = lambda x, u: self.update(self.x, b"s1_init")
        self.s1.entry_action = lambda x, u: self.update(self.x, b"s1_entry")
        self.s1.exit_action = lambda x, u: self.update(self.x, b"s1_exit")
        self.s1.during_action = lambda x, u: self.update(self.x, b"s1_during")

        self.s11.init_action = lambda x, u: self.update(self.x, b"s11_init")
        self.s11.entry_action = lambda x, u: self.update(self.x, b"s11_entry")
        self.s11.exit_action = lambda x, u: self.update(self.x, b"s11_exit")
        self.s11.during_action = lambda x, u: self.update(self.x, b"s11_during")

        self.s2.init_action = lambda x, u: self.update(self.x, b"s2_init")
        self.s2.entry_action = lambda x, u: self.update(self.x, b"s2_entry")
        self.s2.exit_action = lambda x, u: self.update(self.x, b"s2_exit")
        self.s2.during_action = lambda x, u: self.update(self.x, b"s2_during")

        self.s21.init_action = lambda x, u: self.update(self.x, b"s21_init")
        self.s21.entry_action = lambda x, u: self.update(self.x, b"s21_entry")
        self.s21.exit_action = lambda x, u: self.update(self.x, b"s21_exit")
        self.s21.during_action = lambda x, u: self.update(self.x, b"s21_during")

        self.s211.init_action = lambda x, u: self.update(self.x, b"s211_init")
        self.s211.entry_action = lambda x, u: self.update(self.x, b"s211_entry")
        self.s211.exit_action = lambda x, u: self.update(self.x, b"s211_exit")
        self.s211.during_action = lambda x, u: self.update(self.x, b"s211_during")

        super().__init__(self.s0)

    @staticmethod
    def s211_s11_guard(x, u):
        try:
            return u > 10.0
        except:
            return False


    @staticmethod
    def update(x, u):
        x[0].update(u)
        return x

def execute_update_sequence(x, u):
    for action in u:
        x = Example.update(x, action)
    return x

class HSMTest(unittest.TestCase):
 
    # def setUp(self):
 
    #     self.test_hsm = Example()
    
    def test_events_states(self):
        """Test that a sequence of events go to the correct states."""

        # Check the start-up init and entry sequence to end in the right state
        # and having gone through the right sequence
        test_hsm = Example()
        u = [b"s0_entry", b"s0_init", b"s1_entry", b"s1_init", b"s11_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s11)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event a in state s11
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.A)
        u = [b"s11_exit", b"s1_exit", b"s1_entry", b"s1_init", b"s11_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s11)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event e in state s11
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.E)
        u = [b"s11_exit", b"s1_exit", b"s2_entry", b"s21_entry", b"s211_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s211)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event e in state s211
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.E)
        u = [b"s211_exit", b"s21_exit", b"s2_exit", b"s2_entry", b"s21_entry", b"s211_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s211)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event a in state s211
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.A)
        u = []
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s211)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event h in state s211
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.H)
        u = [b"s211_exit", b"s21_exit", b"s21_entry", b"s21_init", b"s211_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s211)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event g in state s211
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.G)
        u = [b"s211_exit", b"s21_exit", b"s2_exit", \
             b"s0_init", b"s1_entry", b"s1_init", b"s11_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s11)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event b in state s11
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.B)
        u = [b"s11_exit", b"s11_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s11)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event g in state s11
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.G)
        u = [b"s11_exit", b"s1_exit", \
             b"s2_entry", b"s21_entry", b"s211_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s211)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event b in state s211
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(Events.B)
        u = [b"s211_exit", b"s21_init", b"s211_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s211)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event None in state s211, with input under 10.0
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(None, 0.0)
        u = []
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s211)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

        # Event None in state s211, with input over 10.0
        test_hsm.x[0] = md5() # Reset the hsm continuous state
        test_hsm.dispatch(None, 20.0)
        u = [b"s211_exit", b"s21_exit", b"s2_exit", b"s1_entry", b"s11_entry"]
        x = execute_update_sequence([md5()], u)
        self.assertIs(test_hsm.state, test_hsm.s11)
        self.assertEqual(test_hsm.x[0].digest(), x[0].digest())
        x[0].update(b"a") # Corrupt the md5 to make sure it works
        self.assertNotEqual(test_hsm.x[0].digest(), x[0].digest())

    def test_unknown_event(self):
        """An unknown event should fail somehow."""
        test_hsm = Example()
        with self.assertRaises(Exception, msg='Attempt use unknown event'):
            test_hsm.dispatch(Events.Z)
        
            
    def test_events_entry_counter_simple(self):
        pass

    def test_events_entry_exit_init(self):
        pass

#################################
### Module Level Test Harness ###
#################################
if __name__ == "__main__":
    # Kick off all unit tests contained in the file
    unittest.main()
